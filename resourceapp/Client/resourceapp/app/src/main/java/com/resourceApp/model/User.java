package com.resourceApp.model;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class User {

    private String name;
    private String email;
    private String _id;
    private int user_level;
    private String password;
    private String created_at;
    private String newPassword;
    private String token;
    private String company_id;
    private JsonArray reports;
    private JsonArray drafts;

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId() {
        return _id;
    }

    public int getUserLevel() { return user_level; }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getCreatedAt() {
        return created_at;
    }

    public String getCompanyId() {
        return company_id;
    }

    public JsonArray getReports() {
        return reports;
    }

    public JsonArray getDrafts() {
        return drafts;
    }


}
