package com.resourceApp.activity_ui_config;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.resourceApp.R;
import com.resourceApp.model.SubFormField;
import com.resourceApp.utils.Utils;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Osman on 30/11/2016.
 * Configuration for programmatically creating widgets
 *  - TextView, EditText, SpinnerView, SwitchView, RadioGroupView
 * If you add further input widgets, you must add them to the inputWidgets array.
 */

public class EditableReportUIConfig extends ReportUIConfig {

    ScrollView scrollView;
    ArrayList<View> inputWidgets; // not including subject/report title
    EditText subject;
    ImageView imageView;
    Button submitBtn;
    ProgressBar progressBar;

    public final int DEFAULT_MARGIN = 25;
    public final int EDIT_TEXT_MARGIN = DEFAULT_MARGIN - 3;
    public final int SPINNER_MARGIN = DEFAULT_MARGIN - 8;
    public final int SECTION_HEADER_FONT_SIZE = 17;
    public final int DEFAULT_FONT_SIZE = 16;

    public EditableReportUIConfig(Context activityContext){
        super(activityContext);
        this.scrollView = new ScrollView(activityContext);
        initLayout();
        inputWidgets = new ArrayList<>();
    }

    public void initLayout() {
        // Configuring the width and height of the linear innerLinearLayout.
        LinearLayout.LayoutParams llLP = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT
        );
        scrollView.setLayoutParams(llLP);
        scrollView.addView(innerLinearLayout);
    }

    public void addSubjectEditTv(String subjectTitle){
        addBlankView(15);
        addFieldHeaderTv("Subject");
        // Hopefully a unique id as 2nd param to preserve state
        subject = getEditTextView(null, 9090909, subjectTitle);
        subject.setMaxLines(1);
        InputFilter[] filters = new InputFilter[1];
        filters[0] = new InputFilter.LengthFilter(50); //Filter to 50 characters
        subject.setFilters(filters);
        subject.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
                | InputType.TYPE_CLASS_TEXT);
        innerLinearLayout.addView(subject);
        addBlankView(5);
    }

    public void addSectionHeaderTv(String sectionTitle){
        if(sectionTitle != null)
            addBlankView(15);
        TextView shtv = getSectionHeaderTv(sectionTitle);
        innerLinearLayout.addView(shtv);
        addBlankView(5);
    }

    private TextView getSectionHeaderTv(String sectionTitle){
        TextView shtv = new TextView(activityContext);
        setMarginsDp(shtv,DEFAULT_MARGIN,0,DEFAULT_MARGIN,0);
        shtv.setText(sectionTitle);
        shtv.setTextSize(SECTION_HEADER_FONT_SIZE);
        shtv.setTextColor(r.getColor(R.color.colorPrimary));
        shtv.setTypeface(Typeface.DEFAULT_BOLD);
        return shtv;
    }

    public void addFieldHeaderTv(String fieldTitle){
        TextView tv = new TextView(activityContext);
        setMarginsDp(tv,DEFAULT_MARGIN,0,DEFAULT_MARGIN,0);
        tv.setText(fieldTitle);
        innerLinearLayout.addView(tv);
    }

    public void addEditTextView(SubFormField field, int id, String textValue) {
        addFieldHeaderTv(field.getFieldTitle());

        EditText et = getEditTextView(field, id, textValue);
        et.setMaxLines(1);
        et.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
                      | InputType.TYPE_CLASS_TEXT);
        innerLinearLayout.addView(et);
        addBlankView(5);

        addInputWidget(et);
    }

    public void addEditTextBoxView(SubFormField field, int id, String textValue) {
        addFieldHeaderTv(field.getFieldTitle());

        EditText et = getEditTextView(field, id, textValue);
        et.setInputType(  InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
                        | InputType.TYPE_CLASS_TEXT
                        | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        et.setMinLines(3);
        innerLinearLayout.addView(et);
        addBlankView(5);

        addInputWidget(et);
    }

    public void addSpinnerView(SubFormField field, int id, String textValue){
        addFieldHeaderTv(field.getFieldTitle());

        Spinner sp = new Spinner(activityContext);
        setMarginsDp(sp, SPINNER_MARGIN,0,SPINNER_MARGIN,0);
        sp.setTag(field.getFieldTitle());
        sp.setId(id);
        ArrayList<String> spOptions = field.getOptions();
        ArrayAdapter<String> spArrayAdapter =
                new ArrayAdapter<>(activityContext, android.R.layout.simple_spinner_dropdown_item, spOptions);
        spArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp.setAdapter(spArrayAdapter);
        sp.setSelection(spArrayAdapter.getPosition(textValue));
        innerLinearLayout.addView(sp);

        addInputWidget(sp);
    }

    public void addSwitchView(SubFormField field, int id, String toggleValue){
        Switch sv = new Switch(activityContext);
        setMarginsDp(sv, DEFAULT_MARGIN, 0, DEFAULT_MARGIN, 0);
        // Passes entire field object as we need to cross reference switch checked state to an option
        // i.e. true refers to options[0] = "Yes" and false refers to options[1] = "No"
        sv.setTag(field);
        sv.setId(id);
        sv.setText(field.getFieldTitle());
        sv.setTextSize(DEFAULT_FONT_SIZE);
        String trueToggleValue = field.getOptions().get(0);
        if(toggleValue.matches(trueToggleValue))
            sv.setChecked(true);
        innerLinearLayout.addView(sv);
        addBlankView(10);

        addInputWidget(sv);
    }

    public void addRadioGroupView(SubFormField field, int id, String savedValue){
        addFieldHeaderTv(field.getFieldTitle());

        RadioGroup rg = new RadioGroup(activityContext);
        rg.setOrientation(RadioGroup.VERTICAL);
        rg.setId(id);
        rg.setTag(field);
        setMarginsDp(rg, DEFAULT_MARGIN, 0, DEFAULT_MARGIN, 0);

        ArrayList<String> radioOptions = field.getOptions();
        int optionsNo = radioOptions.size();
        for(int i = 0; i < optionsNo; i++){
            RadioButton rb = new RadioButton(activityContext);
            String option = radioOptions.get(i);
            rb.setText(option);
            if(option.matches(savedValue))
                rb.setChecked(true);
            rb.setId(i+100);
            rb.setTextSize(DEFAULT_FONT_SIZE);
            rb.setTextColor(r.getColor(R.color.BLACK));
            rg.addView(rb);
        }
        innerLinearLayout.addView(rg);
        addBlankView(10);

        addInputWidget(rg);
    }

    public void addDatePickerDialog(SubFormField field, int id, String savedDate, Context activityContext){
        addFieldHeaderTv(field.getFieldTitle());

        EditText et = new EditText(this.activityContext);
        et.setId(id);
        et.setText("");
        et.append(savedDate);
        et.setTag(field.getFieldTitle());
        et.setFocusable(false);
        setMarginsDp(et, EDIT_TEXT_MARGIN, 0, EDIT_TEXT_MARGIN, 0);
        Calendar myCalendar = Calendar.getInstance();
        String myFormat = "dd/MM/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.UK);

        DatePickerDialog.OnDateSetListener date = (view, year, month, day) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, month);
            myCalendar.set(Calendar.DAY_OF_MONTH, day);
            et.setText(sdf.format(myCalendar.getTime()));
        };

        et.setOnClickListener(v -> {
            // To maintain state of date in DatePickerDialog
            String dateInput = et.getText().toString();
            if(!dateInput.matches("")){
                try {
                    Date etDate = sdf.parse(dateInput);
                    myCalendar.setTime(etDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            new DatePickerDialog(activityContext, date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show();
        });

        innerLinearLayout.addView(et);
        inputWidgets.add(et);
    }

    public void addCurrencyView(SubFormField field, int id, String savedValue) {
        addFieldHeaderTv(field.getFieldTitle());

        EditText et = new EditText(activityContext);
        et.setText("");
        et.append(savedValue); // Sets cursor to end of text
        et.setId(id);
        et.addTextChangedListener(new TextWatcher() {
            private String current = "";
            @Override
            public void beforeTextChanged(CharSequence s, int start, int before, int count) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.toString().equals(current)){
                    et.removeTextChangedListener(this);
                    String replaceable = String.format("[%s,.]",
                            NumberFormat.getCurrencyInstance().getCurrency().getSymbol());
                    String cleanString = s.toString().replaceAll(replaceable, "");
                    double parsed = Double.parseDouble(cleanString);
                    String formatted = NumberFormat.getCurrencyInstance().format((parsed/100));
                    // formatted has the '£' sign included.
                    current = formatted.substring(1);
                    et.setText(current);
                    et.setSelection(current.length());
                    et.addTextChangedListener(this);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) { }
        });
        setMarginsDp(et, EDIT_TEXT_MARGIN, 0, EDIT_TEXT_MARGIN, 0);
        et.setTag(field.getFieldTitle());
        et.setTextSize(DEFAULT_FONT_SIZE);
        et.setTextColor(r.getColor(R.color.BLACK));
        et.setInputType(InputType.TYPE_CLASS_NUMBER);
        et.getBackground().setColorFilter(r.getColor(R.color.editTextUnderlineColor), PorterDuff.Mode.SRC_ATOP);
        innerLinearLayout.addView(et);
        addBlankView(5);

        addInputWidget(et);
    }

    public void addImageView(SubFormField field, int id){
        addFieldHeaderTv(field.getFieldTitle());
        addBlankView(5);

        imageView = new ImageView(activityContext);
        imageView.setId(id);
        imageView.setTag(field.getFieldTitle());
        setMarginsDp(imageView, DEFAULT_MARGIN + 50, 5, DEFAULT_MARGIN + 50, 5);
        imageView.setImageResource(R.drawable.ic_add_photo);
        innerLinearLayout.addView(imageView);

        // Add progressbar set to invisible
        progressBar = Utils.getProgressBar(activityContext);
        innerLinearLayout.addView(progressBar);

        addInputWidget(imageView);
    }

    public void addSubmitButton(){
        addBlankView(15);
        submitBtn = new Button(activityContext);
        setMarginsDp(submitBtn, DEFAULT_MARGIN + 30, 5, DEFAULT_MARGIN + 30, 5);
        submitBtn.setText("Submit report");
        innerLinearLayout.addView(submitBtn);
        addBlankView(25);
    }

    public void addInputWidget(View widget){
        inputWidgets.add(widget);
    }

    public void addSectionBreak(){
        addBlankView(25);
        View topShadow = new View(activityContext);
        // Sets the margin of section break to DEFAULT_MARGIN
        int SECTION_BREAK_MARGIN = DEFAULT_MARGIN - 5;
        LinearLayout.LayoutParams topParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, dpToPx(4));
        topParams.setMargins(dpToPx(SECTION_BREAK_MARGIN),0 , dpToPx(SECTION_BREAK_MARGIN), 0);
        topShadow.setLayoutParams(topParams);
        topShadow.setBackgroundResource(R.drawable.item_shadow_top);
        innerLinearLayout.addView(topShadow);

        View bottomShadow = new View(activityContext);
        LinearLayout.LayoutParams bottomParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, dpToPx(3));
        bottomParams.setMargins(dpToPx(SECTION_BREAK_MARGIN),0 , dpToPx(SECTION_BREAK_MARGIN), 0);
        topShadow.setLayoutParams(bottomParams);
        bottomShadow.setBackgroundResource(R.drawable.item_shadow_bottom);
        innerLinearLayout.addView(bottomShadow);
        addBlankView(5);
    }

    private EditText getEditTextView(SubFormField field, int id, String textValue){
        EditText et = new EditText(activityContext);
        setMarginsDp(et, EDIT_TEXT_MARGIN, 0, EDIT_TEXT_MARGIN, 0);
        if(field != null)
            et.setTag(field.getFieldTitle());
        et.setTextSize(DEFAULT_FONT_SIZE);
        et.setTextColor(r.getColor(R.color.BLACK));
        et.setText("");
        et.setGravity(Gravity.TOP | Gravity.LEFT);
        et.append(textValue); // Sets cursor to end of text
        et.setId(id);
        et.getBackground().setColorFilter(r.getColor(R.color.editTextUnderlineColor), PorterDuff.Mode.SRC_ATOP);
        return et;
    }

    public EditText getSubjectValue(){
        return subject;
    }

    public ScrollView getScrollView() { return scrollView; }

    public ImageView getImageView(){
        return imageView;
    }

    public Button getSubmitBtn(){
        return submitBtn;
    }

    public ArrayList<View> getInputWidgets() { return inputWidgets; }

    public ProgressBar getProgressBar() { return progressBar; }


}
