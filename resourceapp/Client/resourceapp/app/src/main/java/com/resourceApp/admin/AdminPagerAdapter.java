package com.resourceApp.admin;

/**
 * Created by Osman on 09/02/2017.
 */
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class AdminPagerAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;

    public AdminPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new OpenReportListViewFragment();
            case 1:
                return new ClosedReportListViewFragment();
            default:
                return null;
        }
    }

    @Override
    public int getItemPosition(Object item) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}