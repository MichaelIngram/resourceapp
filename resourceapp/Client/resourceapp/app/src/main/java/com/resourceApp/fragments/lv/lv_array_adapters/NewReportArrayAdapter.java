package com.resourceApp.fragments.lv.lv_array_adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.resourceApp.R;
import com.resourceApp.model.SubForm;

import java.util.ArrayList;

public class NewReportArrayAdapter extends ArrayAdapter<SubForm> {
    public NewReportArrayAdapter(Context context, ArrayList<SubForm> subForms) {
        super(context, 0, subForms);
     }

     @Override
     public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
         SubForm subForm = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
           convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_listview_new_report, parent, false);
        }
        TextView tvReportCategory = (TextView) convertView.findViewById(R.id.tvSubFormTitle);
        tvReportCategory.setText(subForm.getCategoryTitle());

        // Return the completed view to render on screen
        return convertView;
    }
}
