package com.resourceApp.model;

import com.google.gson.JsonArray;

public class Company {

    private String name;
    private String email;
    private JsonArray report_templates;
    private int thumbnail;

    public String getName() {
        return name;
    }

    public String getEmail() { return email; }

    public JsonArray getReportTemplates() {
        return report_templates;
    }

    public int getThumbnail() { return thumbnail; }

}
