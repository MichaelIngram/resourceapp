package com.resourceApp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Osman on 14/02/2017.
 */

public class StatusUpdate implements Parcelable {

    private String date;
    private String admin_email;
    private String comment;
    private int status;
    private String dateFormat = "dd MMM yyyy HH:mm";

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.date);
        dest.writeString(this.admin_email);
        dest.writeString(this.comment);
        dest.writeInt(this.status);
    }

    public StatusUpdate(String admin_email, String comment, int status) {
        this.admin_email = admin_email;
        this.comment = comment;
        this.status = status;
        this.date = new SimpleDateFormat(dateFormat).format(new Date());
    }

    protected StatusUpdate(Parcel in) {
        this.date = in.readString();
        this.admin_email = in.readString();
        this.comment = in.readString();
        this.status = in.readInt();
    }

    public static final Parcelable.Creator<StatusUpdate> CREATOR = new Parcelable.Creator<StatusUpdate>() {
        @Override
        public StatusUpdate createFromParcel(Parcel source) {
            return new StatusUpdate(source);
        }

        @Override
        public StatusUpdate[] newArray(int size) {
            return new StatusUpdate[size];
        }
    };

    public String getDate(){
        try{
            Date dateObject = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(date);
            date = new SimpleDateFormat(dateFormat).format(dateObject);
        }
        catch(Exception e){
        }
        return date;
    }

    public String getAdminEmail(){
        return admin_email;
    }

    public String getComment(){
        return comment;
    }

    public int getStatus(){
        return status;
    }

}