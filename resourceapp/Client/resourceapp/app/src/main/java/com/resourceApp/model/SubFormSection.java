package com.resourceApp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Osman on 29/11/2016.
 */

public class SubFormSection implements Parcelable {

    private String sectionTitle;
    private ArrayList<SubFormField> sectionFields;

    public String getSectionTitle() {
        return sectionTitle;
    }

    public ArrayList<SubFormField> getSectionFields() {
        return sectionFields;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.sectionTitle);
        dest.writeTypedList(this.sectionFields);
    }

    public SubFormSection() {
    }

    protected SubFormSection(Parcel in) {
        this.sectionTitle = in.readString();
        this.sectionFields = in.createTypedArrayList(SubFormField.CREATOR);
    }

    public static final Creator<SubFormSection> CREATOR = new Creator<SubFormSection>() {
        @Override
        public SubFormSection createFromParcel(Parcel source) {
            return new SubFormSection(source);
        }

        @Override
        public SubFormSection[] newArray(int size) {
            return new SubFormSection[size];
        }
    };
}
