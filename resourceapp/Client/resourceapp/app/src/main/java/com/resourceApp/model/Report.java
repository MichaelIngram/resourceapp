package com.resourceApp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Osman on 21/11/2016.
 */

public class Report implements Parcelable {

    private String _id;
    private String ref_no;
    private String report_title;
    private String sub_id;
    private String sub_title; // subform title
    private String user_id;
    private String user_email;
    private String company_id;
    private String date_modified;
    private ArrayList<ReportSection> sections;
    private int status;
    private ArrayList<StatusUpdate> statusUpdates;
    private String dateFormat = "dd MMM yyyy HH:mm";

    public Report() {
        _id = "";
        ref_no = "";
        report_title = "";
        sub_id = "";
        sub_title = "";
        user_id = "";
        user_email = "";
        company_id = "";
        date_modified = "";
        sections = new ArrayList<>();
        status = 0;
        statusUpdates = new ArrayList<>();
    }

    public String getId() { return _id; }

    public String getRefNo() {
        return ref_no;
    }

    public String getSubjectTitle() {
        return report_title;
    }

    public String getSubId() {
        return sub_id;
    }

    public String getSubTitle() {
        return sub_title;
    }

    public String getUserId() { return user_id; }

    public String getUserEmail() { return user_email; }

    public String getCompanyId() { return company_id; }

    public String getDateModified() {
        String dateModified;
        try{
            Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(date_modified);
            dateModified = new SimpleDateFormat(dateFormat).format(date);
        }
        catch(Exception e){
            // Dates from drafts and outbox will throw an exception.
            dateModified = date_modified;
            // e.printStackTrace();
        }
        return dateModified;
    }

    public String getStatus() {
        switch(status) {
            case 0:
                return "Closed";
            case 1 :
            default:
                return "Open";
        }
    }

    public int getStatusInt(){
        return status;
    }

    public ArrayList<ReportSection> getSections() {
        return sections;
    }

    public ArrayList<StatusUpdate> getStatusUpdates() { return statusUpdates; }

    public void setRefNo(String refNo){
        this.ref_no = refNo;
    }

    public void setSubjectTitle(String title){
        this.report_title = title;
    }

    public void setSubId(String subId){
        this.sub_id = subId;
    }

    public void setSubTitle(String subTitle){
        this.sub_title = subTitle;
    }

    public void setUserId(String userId){
        this.user_id = userId;
    }

    public void setUserEmail(String userEmail){
        this.user_email = userEmail;
    }

    public void setCompanyId(String companyId){
        this.company_id = companyId;
    }

    public void setDateModified(String date_modified){
        this.date_modified = date_modified;
    }

    public void setSections(ArrayList<ReportSection> sections){
        this.sections = sections;
    }

    public void setStatus(int status) { this.status = status; }

    public void setStatusUpdates(ArrayList<StatusUpdate> statusUpdates) {
        this.statusUpdates = statusUpdates;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this._id);
        dest.writeString(this.ref_no);
        dest.writeString(this.report_title);
        dest.writeString(this.sub_id);
        dest.writeString(this.sub_title);
        dest.writeString(this.user_id);
        dest.writeString(this.user_email);
        dest.writeString(this.company_id);
        dest.writeString(this.date_modified);
        dest.writeTypedList(this.sections);
        dest.writeInt(this.status);
        dest.writeTypedList(this.statusUpdates);
    }

    protected Report(Parcel in) {
        this._id = in.readString();
        this.ref_no = in.readString();
        this.report_title = in.readString();
        this.sub_id = in.readString();
        this.sub_title = in.readString();
        this.user_id = in.readString();
        this.user_email = in.readString();
        this.company_id = in.readString();
        this.date_modified = in.readString();
        this.sections = in.createTypedArrayList(ReportSection.CREATOR);
        this.status = in.readInt();
        this.statusUpdates = in.createTypedArrayList(StatusUpdate.CREATOR);
    }

    public static final Creator<Report> CREATOR = new Creator<Report>() {
        @Override
        public Report createFromParcel(Parcel source) {
            return new Report(source);
        }

        @Override
        public Report[] newArray(int size) {
            return new Report[size];
        }
    };
}
