package com.resourceApp;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.resourceApp.admin.AdminFragment;
import com.resourceApp.fragments.lv.DraftOfflineReportListViewFragment;
import com.resourceApp.fragments.lv.DraftReportListViewFragment;
import com.resourceApp.fragments.lv.NewReportListViewFragment;
import com.resourceApp.fragments.lv.OutboxListViewFragment;
import com.resourceApp.fragments.dialog.SyncDialog;
import com.resourceApp.fragments.lv.SentReportListViewFragment;
import com.resourceApp.fragments.StatsFragment;
import com.resourceApp.model.Company;
import com.resourceApp.model.Report;
import com.resourceApp.model.Response;
import com.resourceApp.model.User;
import com.resourceApp.network.MongoDbConn;
import com.resourceApp.network.S3Conn;
import com.resourceApp.utils.Constants;
import com.resourceApp.utils.ImageUtils;
import com.resourceApp.utils.SyncUtils;
import com.resourceApp.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import retrofit2.adapter.rxjava.HttpException;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.resourceApp.utils.Constants.PREV_VIEWED_MENU_ITEM_ID;

public class NavigationDrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private SharedPreferences mSharedPreferences;
    private String mToken;
    private String mEmail;
    private int mPrevMenuItemId;
    private boolean mProfileSet;
    private boolean mSyncPending;

    DrawerLayout mDrawer;
    NavigationView navigationView;
    Menu optionsMenu;
    View headerView;
    Dialog mOverlayDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);
        mSharedPreferences = Utils.getSharedPrefs(this);
        initSharedPreferences();
        initView();

        // Avoids querying the database if user data is already stored.
        // Only syncs if profile is not set or if a sync has been requested.
        if(!mProfileSet || mSyncPending) {
            syncAll();
        }// Opens "Prev frag" by default
        else if(!mSyncPending && savedInstanceState == null)
            openPrevFrag();

    }

    private void initSharedPreferences() {
        mToken = mSharedPreferences.getString(Constants.TOKEN,"");
        mEmail = mSharedPreferences.getString(Constants.EMAIL,"");
        mPrevMenuItemId = mSharedPreferences.getInt(Constants.PREV_VIEWED_MENU_ITEM_ID, 0);
        mProfileSet = mSharedPreferences.getBoolean(Constants.PROFILE_SET, false);
        mSyncPending = mSharedPreferences.getBoolean(Constants.SYNC_PENDING, false);
    }

    private void initView(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        headerView = navigationView.getHeaderView(0);

        configAdminMenu();
        initCustomText();
        initCustomIcon();
    }

    public void syncAll() {
        //display an invisible overlay dialog to prevent user interaction and pressing back
        //to allow image to be uploaded before showing the synced reports
        mOverlayDialog = Utils.getOverlayDialog(this);
        MongoDbConn.getRetrofit(mToken).getProfile(mEmail)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponseUser,this::handleError);
    }

    private void handleResponseUser(User user) {
        // Stores user profile to shared preferences
        SyncUtils.syncUserSharedPrefs(user, mSharedPreferences);
        // Async call means initView() can execute before handleResponseUser()
        configAdminMenu();
        initCustomText();
        syncCompanyProfile();
    }

    public void syncCompanyProfile() {
        MongoDbConn.getRetrofit()
                .getCompanyProfile(mSharedPreferences.getString(Constants.COMPANY_ID, ""))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponseCompany,this::handleError);
    }

    private void handleResponseCompany(Company company) {
        // Stores all company report templates to shared preferences
        SyncUtils.syncCompanySharedPrefs(company, mSharedPreferences);
        syncSentReports();
        initCustomIcon();
    }

    public void syncSentReports() {
        MongoDbConn.getRetrofit()
                .getSentReports(mSharedPreferences.getString(Constants.USER_ID, ""))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponseSentReports,this::handleError);
    }

    private void handleResponseSentReports(ArrayList<Report> reports) {
        // Stores all user sent reports templates to shared preferences
        SyncUtils.syncSentReportsSharedPrefs(reports, mSharedPreferences);
        openPrevFrag();
        mOverlayDialog.dismiss();
        Utils.showLongToastMessage(getBaseContext(),getString(R.string.sync_success));
    }

    private void handleError(Throwable error) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(Constants.SYNC_PENDING, false);
        editor.apply();

        if (error instanceof HttpException) {
            try {
                Gson gson = new GsonBuilder().create();
                String errorBody = ((HttpException) error).response().errorBody().string();
                Response response = gson.fromJson(errorBody,Response.class);
                showSnackBarMessage(response.getMessage());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        openPrevFrag();
        mOverlayDialog.dismiss();
        Utils.showLongToastMessage(getBaseContext(),getString(R.string.sync_fail));
    }

    private void configAdminMenu() {
        // Displays features only accessible to admins.
        int user_level = mSharedPreferences.getInt(Constants.USER_LEVEL, 0);
        if(user_level == 2){
            Menu navMenu = navigationView.getMenu();
            navMenu.findItem(R.id.nav_admin).setVisible(true);
//            navMenu.findItem(R.id.nav_stats).setVisible(true);
        }
    }

    private void initCustomText(){
        TextView nameDrawerText = (TextView) headerView.findViewById(R.id.nameDrawerText);
        nameDrawerText.setText(mSharedPreferences.getString(Constants.FULL_NAME, "Name"));
        TextView emailDrawerText = (TextView) headerView.findViewById(R.id.emailDrawerText);
        emailDrawerText.setText(mSharedPreferences.getString(Constants.EMAIL, "Email"));
    }

    private void initCustomIcon(){
        // Check if they even have a company icon.
        int thumbnail = mSharedPreferences.getInt(Constants.COMPANY_THUMBNAIL, 0);
        if(thumbnail != 0) {
            ImageView iv = (ImageView) headerView.findViewById(R.id.companyIcon);
            String s3path = ImageUtils.getS3CompanyThumbnailPath(this);
            String companyIconPath = ImageUtils.getInternalImagePath(s3path, this);
            if (new File(companyIconPath).exists()) {
                // Prevents multiple requests on screen rotation, unlocking of phone
                Bitmap bmImg = BitmapFactory.decodeFile(companyIconPath);
                iv.setImageBitmap(bmImg);
                iv.setAdjustViewBounds(false);
            } else {
                S3Conn conn = new S3Conn(getApplicationContext(), this, getBaseContext(), 2);
                ProgressBar progressBar = Utils.getProgressBar(this);
                conn.downloadImageFile(s3path, iv, progressBar);
            }
        }
    }

    private void showSnackBarMessage(String message) {
        Snackbar.make(findViewById(R.id.nav_view), message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.navigation_drawer_options_menu, menu);
        //optionsMenu = menu;
        return true;
    }

    public Menu getOptionsMenu(){
        return optionsMenu;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }
        else if (id == R.id.sync) {
            FragmentManager fm = getSupportFragmentManager();
            SyncDialog alertDialog = SyncDialog.newInstance(getString(R.string.sync_alert_dialog_title));
            alertDialog.show(fm, "fragment_alert");
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int itemId = item.getItemId();
        openNavFragment(itemId);
        if(itemId != R.id.nav_log_out)
            setTitle(item.getTitle());
        mDrawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void openNavFragment(int fragId){

        Fragment fragment = null;
        Class fragmentClass = null;

        switch(fragId) {
            case R.id.nav_new_report:
                fragmentClass = NewReportListViewFragment.class;
                break;
            case R.id.nav_sent_reports:
                fragmentClass = SentReportListViewFragment.class;
                break;
            case R.id.nav_outbox:
                fragmentClass = OutboxListViewFragment.class;
                break;
            case R.id.nav_drafts:
                fragmentClass = DraftReportListViewFragment.class;
                break;
            case R.id.nav_drafts_offline:
                fragmentClass = DraftOfflineReportListViewFragment.class;
                break;
            case R.id.nav_admin:
                fragmentClass = AdminFragment.class;
                break;
            case R.id.nav_stats:
                fragmentClass = StatsFragment.class;
                break;

            case R.id.nav_log_out:
                logout();
                break;
            default:
                fragmentClass = NewReportListViewFragment.class;
                break;
        }

        if(fragmentClass != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            Fragment currentFrag = fragmentManager.findFragmentById(R.id.nav_fragment);

            try {
                fragment = (Fragment) fragmentClass.newInstance();
                // If the same fragment is already open
                if (currentFrag != null) {
                    if (currentFrag.getClass().equals(fragment.getClass()))
                        return;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            // Sets as previous menu item Id.
            SharedPreferences.Editor editor = mSharedPreferences.edit();
            editor.putInt(PREV_VIEWED_MENU_ITEM_ID, fragId);
            editor.apply();
            String itemIdString = Integer.toString(fragId);
            fragmentManager.beginTransaction().replace(R.id.nav_fragment, fragment, itemIdString)
                    .addToBackStack(itemIdString).commit();
        }
    }

    private void openDefaultFrag() {
        navigationView.getMenu().getItem(0).setChecked(true);
        navigationView.getMenu().performIdentifierAction(R.id.nav_new_report, 0);
    }

    private void openPrevFrag(){
        if(mPrevMenuItemId != 0) {
            navigationView.setCheckedItem(mPrevMenuItemId);
            openNavFragment(mPrevMenuItemId);
        }else
            openDefaultFrag();

        hideProgressBar();
    }

    private void hideProgressBar() {
        ProgressBar spinner = (ProgressBar) findViewById(R.id.loading_indicator);
        TextView spinnerText = (TextView) findViewById(R.id.loading_text);
        spinner.setVisibility(View.INVISIBLE);
        spinnerText.setVisibility(View.INVISIBLE);
    }

    private void logout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to log out?");
        builder.setPositiveButton("Yes", (dialog, id) -> {
            SharedPreferences.Editor editor = mSharedPreferences.edit();
            editor.clear();
            editor.apply();
            Intent i = new Intent(NavigationDrawerActivity.this, MainActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
        });
        builder.setNegativeButton("Cancel", (dialog, id) -> dialog.cancel());
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!mSyncPending)
            hideProgressBar();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout layout = (DrawerLayout)findViewById(R.id.drawer_layout);
        if (layout.isDrawerOpen(GravityCompat.START)) {
            layout.closeDrawer(GravityCompat.START);
        }
        else if(getSupportFragmentManager().getBackStackEntryCount() == 0) {
            super.onBackPressed();
        }
        else if(getSupportFragmentManager().getBackStackEntryCount() == 1) {
            moveTaskToBack(false);
        }
        else {
            getSupportFragmentManager().popBackStack();
            // Retrieves tag of currently showing fragment (getBackStackEntryAt())
            // Tag of each fragment has *manually* been set according to the corresponding menu item id.
            // i.e. ViewSubFragment comes under "Sent reports" so has the itemId of the "Sent reports" option
            String menuItemId = getSupportFragmentManager()
                    .getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 2)
                    .getName();
            // Sets selected option in navigation menu accordingly.
            navigationView.setCheckedItem(Integer.parseInt(menuItemId));
        }
    }

}