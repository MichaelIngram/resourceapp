package com.resourceApp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.resourceApp.activity_ui_config.SentReportUIConfig;
import com.resourceApp.model.Report;
import com.resourceApp.model.ReportField;
import com.resourceApp.model.ReportSection;
import com.resourceApp.model.StatusUpdate;
import com.resourceApp.network.S3Conn;
import com.resourceApp.utils.Constants;
import com.resourceApp.utils.ImageUtils;

import java.io.File;
import java.util.ArrayList;

import static com.resourceApp.utils.Utils.allowOrientationChange;

public class SentReportActivity extends AppCompatActivity {

    protected Report report;
    protected Bundle savedState = null;
    protected boolean createdStateInDestroyView;
    protected SentReportUIConfig rvc;
    protected ImageView iv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sent_report);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimaryDark)));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Report details");
        initBundle(savedInstanceState);
        initView();
    }

    private void initView() {
        initAdminHeader();

        TextView refNo = (TextView)findViewById(R.id.tvRefNoValue);
        refNo.setText(report.getRefNo());
        TextView currentStatus = (TextView)findViewById(R.id.tvStatusValueHeader);
        currentStatus.setText(report.getStatus());
        TextView dateSubmitted = (TextView)findViewById(R.id.tvDateValueHeader);
        dateSubmitted.setText(report.getDateModified());
        TextView userEmail = (TextView)findViewById(R.id.tvEmailValueHeader);
        userEmail.setText(report.getUserEmail());

        rvc = new SentReportUIConfig(this);

        ArrayList<ReportSection> sections = report.getSections();
        int sectionsNo = sections.size();

        for (int i = 0; i < sectionsNo; i++){
            ReportSection section = sections.get(i);
            rvc.addBlankView(15);
            rvc.addSectionHeaderTv(section.getSectionTitle());
            rvc.addBlankView(15);

            ArrayList<ReportField> fields = section.getSectionFields();
            int fieldsNo = fields.size();

            for(int j = 0; j < fieldsNo; j++){
                ReportField field = fields.get(j);
                // A field cannot have the word "photo" or "picture" in the title unless it is a photo.
                String fieldTitle = field.getTitle();
                rvc.addFieldHeaderTv(fieldTitle);
                rvc.addBlankView(2);
                String photoCheck = fieldTitle.toLowerCase();
                if(photoCheck.contains("photo") || photoCheck.contains("picture")) {
                    if (field.getValue().matches("1"))
                        addImageView();
                    else {
                        rvc.addFieldValueTv("No photo");
                        rvc.addBlankView(10);
                    }
                }
                else {
                    rvc.addFieldValueTv(field.getValue());
                    rvc.addBlankView(10);
                }
            }
            rvc.addBlankView(25);
            if (i != sectionsNo - 1) {
                rvc.addSectionBreak();
                rvc.addBlankView(5);
            }
        }

        LinearLayout view = (LinearLayout)findViewById(R.id.activity_sent_report_view);
        rvc.finaliseLayout(view);
        Button resolutionHistoryBtn = (Button) findViewById(R.id.resolutionHistoryBtn);
        resolutionHistoryBtn.setOnClickListener(v -> showResolutionHistory());
    }

    // Admin views override this method.
    protected void initAdminHeader(){

    }

    public void addImageView(){
        iv = rvc.getImageView();
        /*
         Check if image is already downloaded, if not download.
          */
        String s3ImagePath = ImageUtils.getS3ImagePath(report.getRefNo(), this);
        String internalPath = ImageUtils.getInternalImagePath(s3ImagePath, this);
        if (new File(internalPath).exists()) {
            Bitmap bmImg = BitmapFactory.decodeFile(internalPath);
            iv.setImageBitmap(bmImg);
            iv.setAdjustViewBounds(true);
            allowOrientationChange(this);
        }
        else {
            S3Conn conn = new S3Conn(getApplicationContext(), this, getBaseContext(), 1);
            conn.downloadImageFile(s3ImagePath, iv, rvc.getProgressBar());
        }
    }

    public void showResolutionHistory(){
        // open new activity.
        // pass in parcelable statusUpdates arraylist.
        Bundle bundle = new Bundle();
        ArrayList<StatusUpdate> statusUpdates = report.getStatusUpdates();
        if(statusUpdates != null)
            bundle.putParcelableArrayList(Constants.REPORT_STATUS_UPDATES, statusUpdates);
        Intent intent = new Intent(this, StatusUpdatesActivity.class);
        intent.putExtra("bundle", bundle);
        this.startActivity(intent);
    }

    private void initBundle(Bundle savedInstanceState) {
        Bundle bundle = getIntent().getBundleExtra("bundle");
        if (savedInstanceState != null) {
            savedState = savedInstanceState.getBundle(Constants.REPORT_INPUT_BUNDLE);
            report = savedState.getParcelable(Constants.REPORT_INPUT_BUNDLE);
        }else
            savedState = bundle;
        if(savedState != null)
            report = savedState.getParcelable(Constants.REPORT_INPUT_BUNDLE);
    }

    private Bundle saveState() {
        Bundle state = new Bundle();
        state.putParcelable(Constants.REPORT_INPUT_BUNDLE, report);
        return state;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (report == null)
            outState.putBundle(Constants.REPORT_INPUT_BUNDLE, savedState);
        else
            outState.putBundle(Constants.REPORT_INPUT_BUNDLE, createdStateInDestroyView ? savedState : saveState());
        createdStateInDestroyView = false;
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }
}
