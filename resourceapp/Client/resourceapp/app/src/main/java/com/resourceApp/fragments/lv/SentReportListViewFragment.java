package com.resourceApp.fragments.lv;

import android.content.Intent;
import android.os.Bundle;
import com.resourceApp.R;
import com.resourceApp.model.Report;
import com.resourceApp.network.MongoDbConn;
import com.resourceApp.utils.Constants;
import com.resourceApp.fragments.lv.lv_array_adapters.ReportArrayAdapter;
import com.resourceApp.SentReportActivity;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.resourceApp.utils.SyncUtils.syncSentReportsSharedPrefs;

public class SentReportListViewFragment extends SwipeReportListViewFragment {

    @Override
    public void initViewParams() {
        // layout_fragment_id, constantsKey, listView_id, swipeLayout_id
        layout_id = R.layout.fragment_sent_reports;
        constantsKey = Constants.SENT_REPORTS_ARRAY;
        listView_id = R.id.lvReports;
        swipeLayout_id = R.id.swipe_sent_reports;
        placeholder_id = R.id.sentPlaceholder;
        getActivity().setTitle("Sent reports");
    }

    @Override
    public void initAdapter() {
        adapter = new ReportArrayAdapter(view.getContext(), reports,
                R.layout.item_listview_sent_report);
    }

    @Override
    public void listViewOptionalInit() {}

    @Override
    public void onItemClick(int pos) {
        Report report = reports.get(pos);
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.REPORT_INPUT_BUNDLE, report);

        Intent intent = new Intent(getActivity(), SentReportActivity.class);
        intent.putExtra("bundle", bundle);
        getActivity().startActivity(intent);
    }

    @Override
    public void refreshRequest() {
        String userId = sp.getString(Constants.USER_ID, "");
        MongoDbConn.getRetrofit().getSentReports(userId)
                .doOnNext(reports -> syncSentReportsSharedPrefs(reports, sp))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(r -> success(), err -> error());
    }

}