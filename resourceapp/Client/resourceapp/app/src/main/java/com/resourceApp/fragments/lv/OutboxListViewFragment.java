package com.resourceApp.fragments.lv;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;

import com.resourceApp.R;
import com.resourceApp.model.Report;
import com.resourceApp.OutboxReportActivity;
import com.resourceApp.utils.Constants;
import com.resourceApp.fragments.lv.lv_array_adapters.ReportArrayAdapter;

public class OutboxListViewFragment extends SwipeReportListViewFragment {

    @Override
    public void initViewParams() {
        constantsKey = Constants.OUTBOX_REPORTS_ARRAY;
        layout_id = R.layout.fragment_outbox;
        listView_id = R.id.lvOutboxReports;
        swipeLayout_id = R.id.swipe_outbox_reports;
        placeholder_id = R.id.outboxPlaceholder;
        getActivity().setTitle("Outbox");
    }

    @Override
    public void initAdapter() {
        adapter = new ReportArrayAdapter(view.getContext(), reports,
                R.layout.item_listview_outbox_report);
    }

    @Override
    public void listViewOptionalInit() {
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView.setMultiChoiceModeListener(new modeCallback());
    }

    @Override
    public void onItemClick(int pos) {
        Report report = reports.get(pos);
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.REPORT_INPUT_BUNDLE, report);

        Intent intent = new Intent(getActivity(), OutboxReportActivity.class);
        intent.putExtra("bundle", bundle);
        getActivity().startActivity(intent);
    }

    @Override
    public void refreshRequest() {
        success(); // illusion that something is happening lol.
    }

    private class modeCallback implements ListView.MultiChoiceModeListener {
        @Override
        public void onItemCheckedStateChanged(ActionMode mode, int pos, long id, boolean check) {
        }

        @Override
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.lv_selection_menu, menu);
            actionMode.setTitle("Select reports to delete");
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.delete_report:
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Are you sure you want to delete the selected reports from your outbox?");
                    builder.setPositiveButton("Yes", (dialog, id) -> {
                        deleteOfflineReports(Constants.OUTBOX_REPORTS_ARRAY,R.id.nav_outbox,"outbox");
                    });
                    builder.setNegativeButton("Cancel", (dialog, id) -> dialog.cancel());
                    AlertDialog alert = builder.create();
                    alert.show();
                    break;
            }
            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode actionMode) {
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
