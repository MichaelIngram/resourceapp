package com.resourceApp.fragments.lv;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.resourceApp.R;
import com.resourceApp.model.Report;
import com.resourceApp.model.SubForm;
import com.resourceApp.DraftReportActivity;
import com.resourceApp.utils.Constants;
import com.resourceApp.fragments.lv.lv_array_adapters.ReportArrayAdapter;

import java.util.ArrayList;

public class DraftOfflineReportListViewFragment extends SwipeReportListViewFragment {

    @Override
    public void initViewParams() {
        // layout_fragment_id, constantsKey, listView_id, swipeLayout_id
        constantsKey = Constants.DRAFT_OFFLINE_REPORTS_ARRAY;
        layout_id = R.layout.fragment_drafts;
        listView_id = R.id.lvDrafts;
        swipeLayout_id = R.id.swipe_drafts;
        placeholder_id = R.id.draftsPlaceholder;
        getActivity().setTitle("Offline drafts");
    }

    @Override
    public void postInitViewParams(){
        TextView header = (TextView) view.findViewById(R.id.tvOfflineDraftsHeader);
        header.setVisibility(View.GONE);
        TextView tv = (TextView) view.findViewById(R.id.draftEmptyText);
        tv.setText("You have no drafts saved offline");
    }

    @Override
    public void initAdapter() {
        adapter = new ReportArrayAdapter(view.getContext(), reports,
                R.layout.item_listview_draft_report);
    }

    @Override
    public void listViewOptionalInit() {
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView.setMultiChoiceModeListener(new modeCallback());
    }

    @Override
    public void onItemClick(int pos){
        // Parceling Report object to send to SentReportActivity
        Report report = reports.get(pos);
        SubForm subForm = getSubForm(report.getSubId());
        Bundle bundle = new Bundle();
        // Draft input
        bundle.putParcelable(Constants.REPORT_INPUT_BUNDLE, report);
        // Report template
        bundle.putParcelable(Constants.REPORT_TEMPLATE_BUNDLE, subForm);

        Intent intent = new Intent(getActivity(), DraftReportActivity.class);
        intent.putExtra("bundle", bundle);
        getActivity().startActivity(intent);
    }

    @Override
    public void refreshRequest() {
        success();
    }

    public SubForm getSubForm(String subId){
        SubForm subForm = null;
        String jsonSubs = sp.getString(Constants.NEW_REPORT_ARRAY, "");
        ArrayList<SubForm> subForms = new Gson().fromJson(jsonSubs,
                new TypeToken<ArrayList<SubForm>>(){}.getType());
        // Simple algorithm of linear time complexity.
        // Performance trade-off is fine as the number of subs per company is unlikely to be very large.
        int subFormNo = subForms.size();
        for (int i = 0; i<subFormNo; i++){
            SubForm currentSub = subForms.get(i);
            if(currentSub.getSubId().matches(subId)){
                subForm = currentSub;
                break;
            }
        }
        return subForm;
    }

    private class modeCallback implements ListView.MultiChoiceModeListener {
        @Override
        public void onItemCheckedStateChanged(ActionMode mode, int pos, long id, boolean check) {
        }

        @Override
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.lv_selection_menu, menu);
            actionMode.setTitle("Select reports");
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.delete_report:
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Are you sure you want to delete the selected offline drafts?");
                    builder.setPositiveButton("Yes", (dialog, id) ->
                            deleteOfflineReports(Constants.DRAFT_OFFLINE_REPORTS_ARRAY,
                                    R.id.nav_drafts_offline, "offline drafts"));
                    builder.setNegativeButton("Cancel", (dialog, id) -> dialog.cancel());
                    AlertDialog alert = builder.create();
                    alert.show();
                    break;
            }
            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode actionMode) {
        }
    }

}