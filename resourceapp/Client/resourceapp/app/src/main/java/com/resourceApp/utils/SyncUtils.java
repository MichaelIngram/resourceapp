package com.resourceApp.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.resourceApp.R;
import com.resourceApp.model.Company;
import com.resourceApp.model.Report;
import com.resourceApp.model.Response;
import com.resourceApp.model.User;
import com.resourceApp.network.MongoDbConn;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static com.resourceApp.utils.Constants.PREV_VIEWED_MENU_ITEM_ID;

/**
 * Created by Osman on 19/01/2017.
 */

public class SyncUtils {

    public static void syncUserProfile(Context activityContext, Context baseContext, ProgressDialog progressDialog){
        SharedPreferences mSharedPreferences = Utils.getSharedPrefs(activityContext);
        String mToken = mSharedPreferences.getString(Constants.TOKEN,"");
        String mEmail = mSharedPreferences.getString(Constants.EMAIL,"");
        MongoDbConn.getRetrofit(mToken).getProfile(mEmail)
                .doOnNext(user -> syncUserSharedPrefs(user, mSharedPreferences))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        user -> {
                            Utils.dismissDialogAndClearStack(progressDialog,activityContext);
                        },
                        throwable -> {
                            Utils.dismissDialogAndClearStack(progressDialog,activityContext);
                            Utils.showLongToastMessage(
                                    baseContext, activityContext.getString(R.string.sync_fail));
                        }
                );
    }

    public static void syncUserSharedPrefs(User user, SharedPreferences sp){
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(Constants.FULL_NAME, user.getName());
        editor.putString(Constants.EMAIL, user.getEmail());
        editor.putInt(Constants.USER_LEVEL, user.getUserLevel());
        editor.putString(Constants.USER_ID, user.getId());
        editor.putString(Constants.COMPANY_ID, user.getCompanyId());
        editor.putString(Constants.DRAFT_REPORTS_ARRAY, user.getDrafts().toString());
        editor.putBoolean(Constants.PROFILE_SET, true);
        editor.putBoolean(Constants.SYNC_PENDING, false);
        editor.apply();
    }

    public static void syncSentReports(Context activityContext, Context baseContext, ProgressDialog progressDialog){
        SharedPreferences mSharedPreferences = Utils.getSharedPrefs(activityContext);
        String userId = mSharedPreferences.getString(Constants.USER_ID, "");
        MongoDbConn.getRetrofit().getSentReports(userId)
                .doOnNext(reports -> syncSentReportsSharedPrefs(reports, mSharedPreferences))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        reports -> {
                            Utils.dismissDialogAndClearStack(progressDialog,activityContext);
                        },
                        throwable -> {
                            Utils.dismissDialogAndClearStack(progressDialog,activityContext);
                            Utils.showLongToastMessage(
                                    baseContext, activityContext.getString(R.string.sync_fail));
                        }
                );
    }

    public static void syncSentReportsSharedPrefs(ArrayList<Report> reports, SharedPreferences sp){
        String json = new Gson().toJson(reports);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(Constants.SENT_REPORTS_ARRAY, json);
        editor.apply();
    }

    public static void syncCompanySharedPrefs(Company company, SharedPreferences mSharedPreferences){
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(Constants.NEW_REPORT_ARRAY, company.getReportTemplates().toString());
        editor.putInt(Constants.COMPANY_THUMBNAIL, company.getThumbnail());
        editor.apply();
    }

    // Accessed by DraftReportListViewFragment and EditableReportActivity.
    public static void deleteDrafts(ArrayList<String> draftIds, boolean fromDraftsFrag,
                             Context activityContext, Context baseContext){
        ProgressDialog progressDialog = Utils.getProgressDialog(activityContext);
        SharedPreferences sp = Utils.getSharedPrefs(activityContext);
        String mEmail = sp.getString(Constants.EMAIL, "");
        /* Forcing previous menu item id to be drafts fragment
        *  Draft fragment may not load on restart otherwise. */
        if(fromDraftsFrag) {
            SharedPreferences.Editor editor = sp.edit();
            editor.putInt(PREV_VIEWED_MENU_ITEM_ID, R.id.nav_drafts);
            editor.apply();
        }

        MongoDbConn.getRetrofit().deleteDrafts(mEmail, draftIds)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        response -> {
                            if(fromDraftsFrag) {
                                SyncUtils.syncUserProfile(activityContext, baseContext, progressDialog);
                                Utils.showLongToastMessage(activityContext, draftIds.size() + " draft(s) deleted.");
                            }
                            // Method to delete images from aws s3 *ONLY* if fromDraftFrag == true
                            // Otherwise you will be deleting from a report that has just been submitted
                            // You only want to delete images from drafts that are directly deleted from the drafts list view.
                            // This is as the submitted report has the same id as the draft.
                        },
                        throwable -> {
                            progressDialog.dismiss();
                            ((Activity)activityContext).setRequestedOrientation(
                                    ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
                            if(fromDraftsFrag)
                                Utils.showLongToastMessage(activityContext, "Unable to delete drafts.");
                        }
                );
    }

    public static void syncOpenReportsSharedPrefs(ArrayList<Report> reports, SharedPreferences sp){
        String json = new Gson().toJson(reports);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(Constants.ADMIN_OPEN_REPORTS, json);
        editor.apply();
    }

    public static void syncClosedReportsSharedPrefs(ArrayList<Report> reports, SharedPreferences sp){
        String json = new Gson().toJson(reports);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(Constants.ADMIN_CLOSED_REPORTS, json);
        editor.apply();
    }

    public static void removeReportFromOutbox(String reportRef, SharedPreferences sp){
        String json = sp.getString(Constants.OUTBOX_REPORTS_ARRAY,"");
        Gson gson = new Gson();
        ArrayList<Report> reportsList = gson.fromJson(json,
                new TypeToken<ArrayList<Report>>(){}.getType());
        int reportNo = reportsList.size();

        for (int i = 0; i < reportNo; i++){
            Report report = reportsList.get(i);
            if(report.getRefNo().matches(reportRef)) {
                reportsList.remove(report);
                break;
            }
        }
        json = gson.toJson(reportsList);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(Constants.OUTBOX_REPORTS_ARRAY, json);
        editor.apply();
    }

}
