package com.resourceApp;

import android.os.Bundle;
import android.widget.ImageView;

import com.resourceApp.EditableReportActivity;
import com.resourceApp.model.Report;
import com.resourceApp.model.SubFormField;
import com.resourceApp.network.MongoDbConn;
import com.resourceApp.network.S3Conn;
import com.resourceApp.utils.ImageUtils;

import java.io.File;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.resourceApp.utils.Utils.allowOrientationChange;

/**
 * Created by Osman on 28/01/2017.
 */

public class DraftReportActivity extends EditableReportActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void initViewParams() {
        activityContext = this;
        draftActivity = true;
    }

    @Override
    public void addImageViewCase(SubFormField field, int id, String savedValue){
        nrv.addImageView(field, id);
        ImageView iv = nrv.getImageView();
        if( savedValue.matches("1")) {
            String s3ImagePath = ImageUtils.getS3ImagePath(reportRefNo, this);
            String internalPath = ImageUtils.getInternalImagePath(s3ImagePath, this);
            if (mCurrentPhotoPath != null || new File(internalPath).exists()) {
                ImageUtils.setImageViewBitmap(iv, internalPath);
                mCurrentPhotoPath = internalPath;
                allowOrientationChange(this);
            }
            else if (mCurrentPhotoPath == null) {
                // Insert here some UI to show the image is downloading.
                S3Conn conn = new S3Conn(getApplicationContext(), this, getBaseContext(), 1);
                conn.downloadImageFile(s3ImagePath, iv, nrv.getProgressBar());
                mCurrentPhotoPath = internalPath;
            }
        }
        nrv.getImageView().setOnClickListener(v -> photoOptionsDialog());
    }

    // Updates existing draft entry
    @Override
    public void saveDraft(String mEmail, Report report) {
        MongoDbConn.getRetrofit().updateDraft(mEmail,report)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(response -> handleResponse(false),
                           err -> handleSaveDraftError(report, err));
    }

}
