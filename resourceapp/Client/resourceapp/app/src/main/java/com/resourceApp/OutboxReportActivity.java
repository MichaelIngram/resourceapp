package com.resourceApp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.resourceApp.R;
import com.resourceApp.SentReportActivity;
import com.resourceApp.model.StatusUpdate;
import com.resourceApp.network.MongoDbConn;
import com.resourceApp.utils.Constants;
import com.resourceApp.utils.Utils;

import java.util.ArrayList;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.resourceApp.utils.Utils.allowOrientationChange;
import static com.resourceApp.utils.Utils.getProgressDialog;
import static com.resourceApp.utils.Utils.preventOrientationChange;
import static com.resourceApp.utils.Utils.showLongToastMessage;

/**
 * Created by Osman on 28/01/2017.
 */

public class OutboxReportActivity extends SentReportActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void initAdminHeader() {
        LinearLayout ll = (LinearLayout)findViewById(R.id.idStatusHeader);
        ll.setVisibility(View.GONE);
    }

}