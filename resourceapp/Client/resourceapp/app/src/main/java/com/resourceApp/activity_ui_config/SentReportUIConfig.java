package com.resourceApp.activity_ui_config;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.resourceApp.R;
import com.resourceApp.utils.Utils;

/**
 * Created by Osman on 30/11/2016.
 */

public class SentReportUIConfig extends ReportUIConfig {

    LinearLayout photoSectionLayout;
    ProgressBar progressBar;

    public final int DEFAULT_MARGIN = 35;
    public final int SECTION_HEADER_FONT_SIZE = 17;
    public final int DEFAULT_FONT_SIZE = 15;

    public SentReportUIConfig(Context activityContext){
        super(activityContext);
    }

    public void finaliseLayout(LinearLayout view) {
        ScrollView scrollView = new ScrollView(activityContext);
        LinearLayout header = (LinearLayout)view.findViewById(R.id.idStatusHeader);
        view.removeView(header);
        LinearLayout reportDetails = getInnerLinearLayout();
        LinearLayout linearChild = getLinearLayout(activityContext);
        linearChild.addView(header);
        linearChild.addView(reportDetails);
        scrollView.addView(linearChild);
        view.addView(scrollView);
    }

    public void addSectionHeaderTv(String sectionTitle){
        TextView shtv = new TextView(activityContext);
        setMarginsDp(shtv,DEFAULT_MARGIN,0,DEFAULT_MARGIN,0);
        shtv.setText(sectionTitle);
        shtv.setTextSize(SECTION_HEADER_FONT_SIZE);
        shtv.setTextColor(r.getColor(R.color.colorPrimary));
        shtv.setTypeface(Typeface.DEFAULT_BOLD);
        innerLinearLayout.addView(shtv);
    }

    public void addFieldHeaderTv(String fieldTitle){
        TextView tv = new TextView(activityContext);
        setMarginsDp(tv,DEFAULT_MARGIN,0,DEFAULT_MARGIN,0);
        tv.setText(fieldTitle);
        tv.setTypeface(Typeface.DEFAULT_BOLD);
        tv.setTextSize(DEFAULT_FONT_SIZE-2);
        innerLinearLayout.addView(tv);
    }

    public void addFieldValueTv(String fieldValue){
        TextView tv = new TextView(activityContext);
        setMarginsDp(tv,DEFAULT_MARGIN,0,DEFAULT_MARGIN,0);
        tv.setText(fieldValue);
        tv.setTextSize(DEFAULT_FONT_SIZE);
        innerLinearLayout.addView(tv);
    }

    public ImageView getImageView(){
        photoSectionLayout = getLinearLayout(activityContext);

        ImageView iv = new ImageView(activityContext);
        iv.setImageResource(R.drawable.ic_report_image);
        setMarginsDp(iv,DEFAULT_MARGIN,0,DEFAULT_MARGIN,0);

        // Add progressbar set to invisible
        progressBar = Utils.getProgressBar(activityContext);

        photoSectionLayout.addView(iv);
        photoSectionLayout.addView(progressBar);
        innerLinearLayout.addView(photoSectionLayout);
        return iv;
    }

    public void addSectionBreak(){
        View topShadow = new View(activityContext);
        // Sets the margin of section break to DEFAULT_MARGIN
        int SECTION_BREAK_MARGIN = DEFAULT_MARGIN - 5;
        LinearLayout.LayoutParams topParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, dpToPx(4));
        topParams.setMargins(dpToPx(SECTION_BREAK_MARGIN),0 , dpToPx(SECTION_BREAK_MARGIN), 0);
        topShadow.setLayoutParams(topParams);
        topShadow.setBackgroundResource(R.drawable.item_shadow_top);
        innerLinearLayout.addView(topShadow);

        View bottomShadow = new View(activityContext);
        LinearLayout.LayoutParams bottomParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, dpToPx(3));
        bottomParams.setMargins(dpToPx(SECTION_BREAK_MARGIN),0 , dpToPx(SECTION_BREAK_MARGIN), 0);
        topShadow.setLayoutParams(bottomParams);
        bottomShadow.setBackgroundResource(R.drawable.item_shadow_bottom);
        innerLinearLayout.addView(bottomShadow);
    }

    public LinearLayout getInnerLinearLayout() {
        return innerLinearLayout;
    }

    public ProgressBar getProgressBar() { return progressBar; }
}
