'use strict';

const user = require('../models/user');
const company = require('../models/company');

exports.getProfile = email => 
	
	new Promise((resolve,reject) => {

		user.find({ email: email }, { name: 1, email: 1, user_level: 1, company_id: 1, reports: 1, drafts: 1, _id: 1 })

		.then(users => resolve(users[0]))

		.catch(err => reject({ status: 500, message: 'Internal Server Error.' }))

	});

exports.getCompanyProfile = companyId => 
	
	new Promise((resolve,reject) => {

		company.find({ _id: companyId }, { report_templates: 1, thumbnail: 1, _id: 0 })

		.then(companies => resolve(companies[0]))

		.catch(err => reject({ status: 500, message: 'Internal Server Error.' }))

	});