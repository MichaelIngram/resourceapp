'use strict';

const auth = require('basic-auth');
const jwt = require('jsonwebtoken');

const register = require('./functions/register');
const login = require('./functions/login');
const profile = require('./functions/profile');
const password = require('./functions/password');
const reports = require('./functions/reports');
const config = require('./config/config.json');

module.exports = router => {

	router.post('/authenticate', (req, res) => {

		const credentials = auth(req);

		if (!credentials) {

			res.status(400).json({ message: 'Invalid Request !' });

		} else {

			login.loginUser(credentials.name, credentials.pass)

			.then(result => {

				const token = jwt.sign(result, config.secret, { expiresIn: "30d" });
			
				res.status(result.status).json({ message: result.message, token: token });

			})

			.catch(err => res.status(err.status).json({ message: err.message }));
		}
	});

	router.post('/users', (req, res) => {

		const name = req.body.name;
		const email = req.body.email;
		const password = req.body.password;

		if (!name || !email || !password || !name.trim() || !email.trim() || !password.trim()) {

			res.status(400).json({message: 'Invalid Request !'});

		} else {

			register.registerUser(name, email, password)

			.then(result => {

				res.setHeader('Location', '/users/'+email);
				res.status(result.status).json({ message: result.message })
			})

			.catch(err => res.status(err.status).json({ message: err.message }));
		}
	});
    

	router.get('/users/:id', (req,res) => {

		if (checkToken(req)) {

			profile.getProfile(req.params.id)

			.then(result => res.json(result))

			.catch(err => res.status(err.status).json({ message: err.message }));

		} else {

			res.status(401).json({ message: 'Invalid token. Please logout and log back in.' });
		}
	});
    
    router.get('/reports/:id', (req,res) => {
        
        reports.getSentReports(req.params.id)

        .then(result => res.json(result))

        .catch(err => res.status(err.status).json({ message: err.message }));

	});
    
    router.get('/companies/:id', (req,res) => {
        
        profile.getCompanyProfile(req.params.id)

        .then(result => res.json(result))

        .catch(err => res.status(err.status).json({ message: err.message }));

	});
    
    router.get('/admin/open/:id', (req,res) => {
        
        reports.getOpenReports(req.params.id)

        .then(result => res.json(result))

        .catch(err => res.status(err.status).json({ message: err.message }));

	});
    
    router.get('/admin/closed/:id', (req,res) => {
        
        reports.getClosedReports(req.params.id)

        .then(result => res.json(result))

        .catch(err => res.status(err.status).json({ message: err.message }));

	});

	router.put('/users/:id', (req,res) => {

		if (checkToken(req)) {

			const oldPassword = req.body.password;
			const newPassword = req.body.newPassword;

			if (!oldPassword || !newPassword || !oldPassword.trim() || !newPassword.trim()) {

				res.status(400).json({ message: 'Invalid Request !' });

			} else {

				password.changePassword(req.params.id, oldPassword, newPassword)

				.then(result => res.status(result.status).json({ message: result.message }))

				.catch(err => res.status(err.status).json({ message: err.message }));

			}
		} else {

			res.status(401).json({ message: 'Invalid Token !' });
		}
	});
    
    router.post('/reports/:id', (req,res) => {
        
        const reportJson = req.body;
                
        reports.sendReport(reportJson)

        .then(result => res.status(result.status).json({ message: result.message+". Report submitted." }))

        .catch(err => res.status(err.status).json({ message: err.message }));
        
	});
    
    router.put('/drafts/new/:id', (req,res) => {
        
        const reportJson = req.body;
        
        reports.saveDraft(req.params.id, reportJson)

        .then(result => res.status(result.status).json({ message: result.message+". Draft saved." }))

        .catch(err => res.status(err.status).json({ message: err.message }));

	});
    
    router.put('/drafts/update/:id', (req,res) => {
        
        const reportJson = req.body;
        
        reports.updateDraft(req.params.id, reportJson)

        .then(result => res.status(result.status).json({ message: result.message+". Draft saved." }))

        .catch(err => res.status(err.status).json({ message: err.message }));

	});
    
    router.put('/drafts/delete/:id', (req,res) => {
        
        const draftIds = req.body;
        
        reports.deleteDrafts(req.params.id, draftIds)

        .then(result => res.status(result.status).json({ message: result.message+". Draft(s) deleted." }))

        .catch(err => res.status(err.status).json({ message: err.message }));

	});
    
    router.put('/admin/addReportStatus/:id', (req,res) => {
        
        var statusUpdateObject = req.body;
        
        statusUpdateObject.date = new Date();
                
        reports.addReportStatus(req.params.id, statusUpdateObject)

        .then(result => res.status(result.status).json({ message: result.message+". Status updated." }))

        .catch(err => res.status(err.status).json({ message: err.message }));

	});
    
    router.get('/admin/getReportStatusUpdates/:id', (req,res) => {
        
        reports.getReportStatusUpdates(req.params.id)

        .then(result => res.json(result))

        .catch(err => res.status(err.status).json({ message: err.message }));

	});
    
	router.post('/users/:id/password', (req,res) => {

		const email = req.params.id;
		const token = req.body.token;
		const newPassword = req.body.password;

		if (!token || !newPassword || !token.trim() || !newPassword.trim()) {

			password.resetPasswordInit(email)

			.then(result => res.status(result.status).json({ message: result.message }))

			.catch(err => res.status(err.status).json({ message: err.message }));

		} else {

			password.resetPasswordFinish(email, token, newPassword)

			.then(result => res.status(result.status).json({ message: result.message }))

			.catch(err => res.status(err.status).json({ message: err.message }));
		}
	});

	function checkToken(req) {

		const token = req.headers['x-access-token'];

		if (token) {

			try {

  				var decoded = jwt.verify(token, config.secret);

  				return decoded.message === req.params.id;

			} catch(err) {

				return false;
			}

		} else {

			return false;
		}
	}
}